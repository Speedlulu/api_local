import re
try:
    import readline
except ModuleNotFoundError:
    # QoL module, not available on Windows
    pass
from pathlib import Path


class QueryCompleter():
    def __init__(self, methods, routes, requests_params):
        self.methods = methods
        self.routes = routes
        self.requests_params = requests_params
        self.matches = []
        self.route_args_matches = []

    @staticmethod
    def _replace_patterns(endpoint):
        endpoint = re.sub(r'^/', r'/?', endpoint)
        endpoint = re.sub(
            r'<int:(?P<name>[^: ]+)>',
            lambda m: r'(?P<%s>\d+)' % m.groupdict()['name'],
            endpoint
        )
        endpoint = re.sub(
            r'<path:(?P<name>[^: ]+)>',
            lambda m: r'(?P<%s>[^ ]+)' % m.groupdict()['name'],
            endpoint
        )
        endpoint = re.sub(
            r'(?<!\?P)<(?P<name>[^/ \?]+)>',
            lambda m: r'(?P<%s>[^/ ]+)' % m.groupdict()['name'],
            endpoint
        )
        return endpoint

    @staticmethod
    def _partial_match(pattern, string):
        tokens = re.sre_parse.parse(pattern)

        match = None

        for i in range(1, len(tokens) + 1):
            _match = re.sre_compile.compile(tokens[:i]).match(string)
            if _match is None:
                return match
            else:
                if _match.end() == len(string):
                    match = _match
        else:
            return match

    def complete(self, text, state):
        words = readline.get_line_buffer().split()
        begin = readline.get_begidx()

        if state == 0:
            # first word, HTTP method
            if len(words) <= 1 and begin == 0:
                if text:
                    self.matches = [
                        s for s in self.methods
                        if s and s.startswith(text.upper())
                    ]
                else:
                    self.matches = self.methods
            # second word, endpoint
            elif len(words) <= 2 and begin != 0:
                if text:
                    self.matches.clear()
                    for key, value in self.routes.items():
                        if words[0].upper() in value['methods']:
                            m = self._partial_match(
                                self._replace_patterns(key),
                                text
                            )
                            if m is not None:
                                if len(m.groupdict()) != 0:
                                    current_route = key
                                    for m_key, m_val in m.groupdict().items():
                                        if m_val is None:
                                            continue
                                        current_route = re.sub(
                                            '<([^ ]+:)?%s>' % m_key,
                                            m_val,
                                            current_route
                                        )
                                    else:
                                        self.matches.append(current_route)
                                else:
                                    self.matches.append(key)
                else:
                    self.matches = [
                        key
                        for key, value in self.routes.items()
                        if key and words[0].upper() in value['methods']
                    ]
        try:
            response = self.matches[state]
        except IndexError:
            response = None
        return response


def load_history(history_filepath):
    history_file = str(Path(history_filepath).expanduser().resolve())
    try:
        readline.read_history_file(history_file)
        history_len = readline.get_current_history_length()
    except FileNotFoundError:
        open(history_file, 'wb').close()
        history_len = 0
    return history_len


def write_history(history_len, history_filepath):
    history_file = str(Path(history_filepath).expanduser().resolve())
    readline.set_history_length(1000)
    readline.append_history_file(
        readline.get_current_history_length() - history_len,
        history_file
    )


def setup_completion(methods=[], routes={}, params=[]):
    readline.parse_and_bind('tab: complete')
    readline.set_completer_delims(' ')
    readline.set_completer(
        QueryCompleter(
            methods,
            routes,
            params,
        ).complete
    )
