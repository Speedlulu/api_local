#!/usr/bin/python3

import sys
import os

from configparser import ConfigParser
from pathlib import Path
from shutil import copyfile
from subprocess import CalledProcessError, check_output

import colorama
from termcolor import colored


__all__ = ['config', ]


colorama.init()


class Config:
    def __init__(self, config_entries):
        self.config_entries = config_entries

    def __getattr__(self, name):
        value = self.config_entries.get(name)
        if value is None:
            pass
        elif value.startswith('`') and value.endswith('`'):
            try:
                value = check_output(
                    value[1:-1],
                    shell=True
                ).decode('utf-8').rstrip('\n')
            except CalledProcessError as e:
                print(colored(e, 'red'))
                sys.exit(-1)
        # integer
        elif value.isnumeric():
            value = self.config_entries.getint(name)
        # boolean
        elif value in ConfigParser.BOOLEAN_STATES:
            value = self.config_entries.getboolean(name)
        # none of the above
        setattr(self, name, value)
        return value


config_path = Path(os.path.join(os.path.dirname(__file__), 'config.ini'))
if not config_path.exists():
    print(colored('Config file not found, creating it', 'yellow'))
    example_config_path = Path(
        os.path.join(
            os.path.dirname(__file__),
            'config_example.ini'
        )
    )
    if not example_config_path.exists():
        print(
            colored(
                "No example config can't create default config aborting",
                'red'
            )
        )
        sys.exit(-1)
    copyfile(example_config_path.absolute(), config_path.absolute())

config_parser = ConfigParser()
config_parser.read(config_path.absolute())
config = Config(config_parser['config'])
