#! /usr/bin/env python3

import argparse
import ast
import json
import re
from queue import SimpleQueue
from sys import exit

import colorama
from pygments import highlight
from pygments.formatters import TerminalFormatter
from pygments.lexers import JsonLexer
from requests import Session
from requests.exceptions import ConnectionError
from termcolor import colored

import completion
from config import config
from request_builder import ApiRequest


HTTP_METHODS = ('DELETE', 'GET', 'OPTIONS', 'PATCH', 'POST', 'PUT', 'HEAD', )
REQUEST_PARAMS = (
    'method', 'url', 'headers', 'files', 'data', 'params',
    'auth', 'cookies', 'hooks', 'json'
)


class InteractiveException(Exception):
    COLOR = None

    def __init__(self, e_string, origin_exception=None):
        super(InteractiveException, self).__init__(e_string)
        self.origin_exception = origin_exception

    def __str__(self):
        return colored(
            super(InteractiveException, self).__str__(),
            self.COLOR
        )

    def str_origin_exception(self):
        if self.origin_exception is None:
            return None
        return colored(
            str(self.origin_exception),
            self.COLOR
        )


class InteractiveWarning(InteractiveException):
    COLOR = 'yellow'


class InteractiveError(InteractiveException):
    COLOR = 'red'


class InteractiveQuerier():
    HEADER_COLOR = 'blue'
    COOKIE_COLOR = 'blue'
    BODY_COLOR = 'blue'
    REQUEST_COLOR = 'magenta'
    OK_COLOR = 'green'
    NOK_COLOR = 'red'

    def __init__(self, **kwargs):
        self.verbose = kwargs.get('verbose')
        self.base_url = kwargs.get('base_url')
        self.local = kwargs.get('local')
        self.history_file = kwargs.get('history_file')
        if self.local:
            self.base_url = kwargs.get('local_base_url')
        self.no_completion = kwargs.get('no_completion')
        self.routes_endpoint = kwargs.get('routes_endpoint')
        self.session = Session()
        self.query_queue = SimpleQueue()

    def parse_query(self, query):
        ARG_REGEX = re.compile(
            r'%s %s' % (
                r"(?P<key>\w+)\s?=\s?(?P<value>(\d|'.*?'|",
                r'\".*?\"|\[.*?\]|{.*?}))'
            )
        )

        try:
            method, target, *payload = query.split()
        except ValueError:
            raise InteractiveWarning('Could not find method and target')

        if method.upper() not in HTTP_METHODS:
            raise InteractiveWarning('Method not recognized')

        if len(target) > 1 and target[-1] == '/':
            target = target[:-1]
        if target[0] != '/':
            target = '/' + target

        payload = ' '.join(payload)

        arg_dict = dict()
        for arg in ARG_REGEX.finditer(payload):
            try:
                arg_dict[arg.groupdict()['key']] = ast.literal_eval(
                    arg.groupdict()['value']
                )
            except (ValueError, SyntaxError) as e:
                raise InteractiveError(
                    'Malaformed expression in arg %s' % arg.groupdict()['key'],
                    origin_exception=e
                )

        if len(arg_dict) != payload.count('='):
            print(
                InteractiveWarning(
                    'Some arguments have not been taken into account'
                )
            )

        return {
            'method': method,
            'target': target,
            'payload': arg_dict,
        }

    def create_request(self, query=None, **kwargs):
        if query is not None:
            query_dict = self.parse_query(query)
            kwargs['method'] = query_dict['method']
            kwargs['url'] = self.base_url + query_dict['target']
            kwargs.update(query_dict['payload'])

        try:
            request = ApiRequest(**kwargs)
        except TypeError as e:
            match = re.search(r"([^']+?)'$", e.args[0])
            if match:
                raise InteractiveWarning(
                    '%s is not a correct request argument' % match.group(1)
                )
            raise InteractiveWarning(str(e))

        return request

    def handle_response(self, response):
        print('Response [%s]' % colored(
            response.status_code,
            self.OK_COLOR if response.ok else self.NOK_COLOR
        ))

        if self.verbose:
            print(colored('Headers :', self.HEADER_COLOR))
            print(colored(response.headers, self.HEADER_COLOR))

            if response.cookies:
                print(colored('Cookies :', self.COOKIE_COLOR))
                print(colored(response.cookies.get_dict(), self.COOKIE_COLOR))

        for cookie in response.cookies:
            self.session.cookies.set_cookie(cookie)

        if response.request.method == 'HEAD':
            for key, value in response.headers.items():
                print(colored(key, attrs=['bold']), end=': ')
                print(value)
        elif response.headers.get('Content-Type') == 'application/json':
            j = json.loads(response.text)
            if j.get('response_firebase'):
                status_code = j['response_firebase']['status_code']
                print(
                    '\tFirebase [%s]' % colored(
                        status_code,
                        self.NOK_COLOR if status_code > 200 else self.OK_COLOR
                    )
                )
                print(
                    '\t' + '\t'.join(
                        j['response_firebase']['text'].splitlines(True)
                    )
                )
            elif j.get('confirmation') is not None:
                code = input(j['confirmation']['value'] + ' : ')
                return {
                    'action': 'replay_request',
                    'headers': {
                        'X-Confirmation-Code': code
                    }
                }
            else:
                print(
                    highlight(
                        json.dumps(
                                j,
                                indent=2,
                            ),
                        JsonLexer(),
                        TerminalFormatter()
                    )
                )
        else:
            print(response.text)

    def display_request(self, prepared_request):
        print(
            colored(
                '%s %s' % (
                    prepared_request.method,
                    prepared_request.path_url
                ),
                self.REQUEST_COLOR
            )
        )
        if self.verbose:
            print(colored('Headers :', self.HEADER_COLOR))
            print(colored(prepared_request.headers, self.HEADER_COLOR))
            if prepared_request.body is not None:
                print(colored('Body :', self.BODY_COLOR))
                print(colored(prepared_request.body, self.BODY_COLOR))
            if len(prepared_request._cookies.get_dict()) != 0:
                print(colored('Cookies : ', self.COOKIE_COLOR))
                print(
                    colored(
                        prepared_request._cookies.get_dict(),
                        self.COOKIE_COLOR
                    )
                )

    def process_requests(self):
        while not self.query_queue.empty():
            request = self.query_queue.get_nowait()
            prepared_request = self.session.prepare_request(request)
            self.display_request(prepared_request)
            try:
                response = self.session.send(
                    prepared_request
                )
            except ConnectionError:
                print(InteractiveError('Could not connect to API server'))
                continue

            response_return = self.handle_response(response)

            if response_return is not None:
                action = response_return.pop('action', None)
                if action == 'replay_request':
                    for key, value in response_return.items():
                        request_attr = getattr(
                            request,
                            key,
                            None
                        )
                        if request_attr is None:
                            continue
                        if getattr(request_attr, 'update', None) is not None:
                            request_attr.update(value)
                        else:
                            setattr(request, key, value)

                    self.query_queue.put_nowait(request)

    def main(self):
        try:
            history_len = completion.load_history(self.history_file)

            if not self.no_completion:
                try:
                    routes_response = self.session.send(
                        self.session.prepare_request(
                            ApiRequest(
                                method='get',
                                url=self.base_url + self.routes_endpoint
                            )
                        )
                    )
                    routes = routes_response.json()['content']
                except Exception as e:
                    print('Error during routes fetch')
                    print(e)
                    return -1

                completion.setup_completion(
                    methods=HTTP_METHODS,
                    routes=routes,
                    params=REQUEST_PARAMS,
                )

            while 1:
                self.process_requests()
                query = input('%s> ' % self.base_url)
                try:
                    self.query_queue.put_nowait(
                        self.create_request(query)
                    )
                except InteractiveException as e:
                    print(e)
                    if self.verbose and e.origin_exception is not None:
                        print(e.str_origin_exception())
                    continue
        except (EOFError, KeyboardInterrupt):
            completion.write_history(history_len, self.history_file)
            return 0

    def __call__(self):
        return self.main()

    def __str__(self):
        return 'Interactive API querier on %s' % self.base_url


# ==========

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interactive API querier')

    DEFAULT_HISTFILE = '.request_histfile'

    parser.add_argument(
        '-v',
        '--verbose',
        help='Increase output verbosity',
        action='store_true'
    )
    parser.add_argument(
        '--local',
        help='Run on local server',
        action='store_true'
    )
    parser.add_argument(
        '--base-url',
        help='Base URL',
        default=config.base_url,
        required=config.base_url is None,
    )
    parser.add_argument(
        '--local-base-url',
        help='Local base URL',
        default=config.local_base_url,
    )
    parser.add_argument(
        '--history-file',
        help='History file path',
        default=config.history_file or DEFAULT_HISTFILE,
    )
    parser.add_argument(
        '--routes-endpoint',
        help='Endpoint to get routes info used for completion',
        default=config.routes_endpoint,
    )
    parser.add_argument(
        '--no-completion',
        help='Disable completion',
        action='store_true'
    )

    args = parser.parse_args()

    if args.local and (
        args.local_base_url is None and config.local_base_url is None
    ):
        parser.error('No local base URL supplied')

    if config.routes_endpoint is None:
        print('WARNING: No routes endpoint disabling completion')
        args.no_completion = True

    querier = InteractiveQuerier(**dict(args._get_kwargs()))

    colorama.init()

    exit(querier())
