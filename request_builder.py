from base64 import b64encode
from os.path import expanduser
from pathlib import Path

from Cryptodome.PublicKey import RSA
from Cryptodome.Signature import PKCS1_v1_5
from Cryptodome.Hash import SHA256
from requests import Request

from config import config


class ApiRequest(Request):
    def make_signature(self):
        string_to_sign = self.method.upper() + ' ' + self.url
        if string_to_sign[-1] == '/':
            string_to_sign = string_to_sign[:-1]
        key = RSA.importKey(
            Path(
                expanduser(config.ssh_key_file)
            ).open().read()
        )
        string_hash = SHA256.new(string_to_sign.encode('utf-8'))
        signature = PKCS1_v1_5.new(key).sign(string_hash)
        return b64encode(signature).decode('utf-8')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.headers.update(
            {
                "X-API-Signature": self.make_signature()
            }
        )

    def __str__(self):
        return self.method.upper() + ' ' + self.url
